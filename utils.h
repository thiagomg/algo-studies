#pragma once

#include <cxxabi.h>

template<typename Class>
std::string get_class_name(Class &c) {
    int status;
    char * demangled = abi::__cxa_demangle(typeid(Class).name(),0,0,&status);
    return demangled;
}
