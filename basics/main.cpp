#include <iostream>

#include "linked_list.h"
#include "u_btree.h"

//Not using our vector.

int main()
{
    tmg_test::test_linked_list();
    tmg_test::test_u_btree();

    return 0;
}
