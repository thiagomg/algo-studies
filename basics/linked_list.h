#ifndef LINKED_LIST_H
#define LINKED_LIST_H

#pragma once

#include <utility>

namespace tmg {

template <typename T>
struct node_linked_list {
    node_linked_list<T> *_next = nullptr;
    T _value;

    node_linked_list(T &value) {
        _value = std::move(value);
    }

    T& value() {
        return _value;
    }

    bool has_next() {
        return _next != nullptr;
    }

    node_linked_list<T> *next() {
        return _next;
    }

};

template <typename T>
struct linked_list {
    node_linked_list<T> *_first = nullptr;
    node_linked_list<T> *_last = nullptr;

    ~linked_list() {
        while(_first != nullptr) {
            node_linked_list<T> *next = _first->next();
            delete _first;
            _first = next;
        }
    }

    node_linked_list<T> *first() {
        return _first;
    }

    node_linked_list<T> *last() {
        return _last;
    }

    void push_back(T &&value) {
        node_linked_list<T> *n = new node_linked_list<T>(value);
        if(_first == nullptr) {
            _first = n;
        }
        if(_last == nullptr) {
            _last = n;
        } else {
            _last->_next = n;
            _last = n;
        }
    }

    void push_front(T &&value) {
        node_linked_list<T> *n = new node_linked_list<T>(value);
        n->_next = _first;
        _first = n;
    }

};

}

namespace tmg_test {

#include <iostream>
void test_linked_list() {

    tmg::linked_list<int> l;
    l.push_back(20);
    l.push_back(30);
    l.push_back(40);
    l.push_front(10);
    l.push_front(5);

    auto first = l.first();
    while(first != nullptr) {
        std::cout << first->value() << std::endl;
        first = first->next();
    }

}

}
#endif // LINKED_LIST_H
