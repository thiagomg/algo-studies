#pragma once

struct union_find_bal {
    vector<int> data;
    vector<int> tree_len;

    union_find_bal(int size) : data(size), tree_len(size) {
	for(int i=0; i < size; ++i) {
	    data[i]=i;
	    tree_len[i]=1;
	}
    }

    int root(int p) {
	//if the value of the position is equal to the position, we stop
	//otherwise, we move to the position pointed by the value
	int cur = p;
	while(true) {
	    if(cur == data[cur]) {
		return cur;
	    }
	    cur = data[cur];
	}
	
	cout << "ERROR!!! : root(" << p << ")" << endl;
	return -1;
    }
    
    void connect(int i1, int i2) {
	int r1 = root(i1);
	int r2 = root(i2);

        if( r1 == r2 ) return;
        
	if(tree_len[r1] < tree_len[r2]) {
	    data[r1] = r2;
	    tree_len[r2] += tree_len[r1];
	} else {
	    data[r2] = r1;
	    tree_len[r1] += tree_len[r2];
	}
    }

    bool connected(int i1, int i2) {
	int r1 = root(i1);
	int r2 = root(i2);
	return 	data[r1] == data[r2];
    }
    
    int depth() {
        //This is wrong. we need to get the root count
        int l = 0;
        cout << "sizes: ";
        for(auto i : tree_len) {
            l = max(l, i);
            cout << i << " ";
        }
        cout << endl;
        return l;
    }

};
