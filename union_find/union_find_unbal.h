#pragma once

struct union_find_unbal {
    vector<int> data;

    union_find_unbal(int size) : data(size) {
	for(int i=0; i < size; ++i) {
	    data[i]=i;
	}
    }

    int root(int p) {
	//if the value of the position is equal to the position, we stop
	//otherwise, we move to the position pointed by the value
	int cur = p;
	while(true) {
	    if(cur == data[cur]) {
		return cur;
	    }
	    cur = data[cur];
	}
	
	cout << "ERROR!!! : root(" << p << ")" << endl;
	return -1;
    }
    
    void connect(int i1, int i2) {
	int r1 = root(i1);
	int r2 = root(i2);
	data[r1] = data[r2];
    }

    bool connected(int i1, int i2) {
	int r1 = root(i1);
	int r2 = root(i2);
	return 	data[r1] == data[r2];
    }

};
