#include <iostream>
#include <fstream>

#include <algorithm>
#include <vector>
#include <string>

#include "../utils.h"

#include "union_find_quad.h"
#include "union_find_unbal.h"
#include "union_find_bal_mem.h"
#include "union_find_bal.h"

#define class_name(x) #x

using namespace std;

vector<pair<int,int>> read_pairs(const std::string &file_name) {

    vector<pair<int,int>> ret;
  
    ifstream f(file_name, std::ifstream::in);

    int count;
    while(!f.eof()) {
	int x, y;
	f >> x;
	f >> y;
	if(!f.good()) {
	    break;
	}
	ret.push_back(make_pair(x, y));
    }

    return ret;
}

int max_value(const vector<pair<int,int>> &lines) {
    int ret = 0;
    for(auto &p : lines) {
	int v = max(p.first, p.second);
	ret = max(v, ret);
    }
    return ret;
}

void print(const vector<pair<int,int>> &lines) {
    for(auto &p : lines) {
	cout << "(" << p.first << "," << p.second << ")" << endl;
    }
    cout << endl;
}

void print(const vector<int> &lines) {
    cout << "( ";
    for(auto &p : lines) {
	cout << p << " ";
    }
    cout << ")" << endl;
}

template<typename QuickFind>
void run(const vector<pair<int,int>> &lines, QuickFind &qf) {

    //We start in zero, hence we need +1 as we got the max value, not the size
    //quick_find qf(max_value(lines) + 1);
    for(const auto &p : lines) {
	qf.connect(p.first, p.second);
    }

    cout << "resulting vector: " << get_class_name(qf) << endl;
    print(qf.data);
    cout << endl;

    // cout << "=> Tests for input1.txt ==========================" << endl;
    // cout << "has to be true -> " << qf.connected(0, 5) << endl;
    // cout << "has to be true -> " << qf.connected(0, 6) << endl;
    // cout << "has to be true -> " << qf.connected(1, 7) << endl;
    // cout << "has to be true -> " << qf.connected(3, 4) << endl;
    // cout << "has to be true -> " << qf.connected(8, 9) << endl;    
    // cout << "has to be false -> " << qf.connected(0, 1) << endl;
    // cout << "has to be false -> " << qf.connected(0, 9) << endl;
    // cout << "has to be false -> " << qf.connected(2, 3) << endl;
    // cout << "has to be false -> " << qf.connected(1, 4) << endl;

    // cout << endl;
    // cout << "=> For comparing ================================" << endl;
    // cout << "max depth: " << qf.depth() << endl;
    
}

//This implementation does some partial balancing.
//It is more memory efficient for cases with very big structures
int main(int argc, char **argv) {
    string file_name = argv[1];
    auto lines = read_pairs(file_name);

    cout << "lines read from file:" << endl;
    print(lines);

    int array_size = max_value(lines) + 1;
    
    quick_find_quad qf(array_size);
    run(lines, qf); 

    union_find_unbal uf_unbal(array_size);
    run(lines, uf_unbal); 

    union_find_bal_mem uf_bal_mem(array_size);
    run(lines, uf_bal_mem); 

    union_find_bal uf_bal(array_size);
    run(lines, uf_bal); 

    return 0;
}
