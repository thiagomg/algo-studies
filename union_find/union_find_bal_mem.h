#pragma once

struct union_find_bal_mem {
    vector<int> data;

    union_find_bal_mem(int size) : data(size) {
	for(int i=0; i < size; ++i) {
	    data[i]=i;
	}
    }

    pair<int, int> root(int p) {
	//if the value of the position is equal to the position, we stop
	//otherwise, we move to the position pointed by the value
	int cur = p;
	int count = 0;
	while(true) {
	    if(cur == data[cur]) {
		return make_pair(cur, count);
	    }
	    cur = data[cur];
	    ++count;
	}
	
	cout << "ERROR!!! : root(" << p << ")" << endl;
	return make_pair(-1, -1);
    }
    
    void connect(int i1, int i2) {
	pair<int, int> r1 = root(i1);
	pair<int, int> r2 = root(i2);
	if(r1.second > r2.second)
	    data[r1.first] = data[r2.first];
	else
	    data[r2.first] = data[r1.first];
    }

    bool connected(int i1, int i2) {
	pair<int, int> r1 = root(i1);
	pair<int, int> r2 = root(i2);
	return 	data[r1.first] == data[r2.first];
    }

};
